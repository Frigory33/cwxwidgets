BIN := _bin/libcwx.so
SRC := $(wildcard src/*.c src/*.cpp)
OBJ := $(SRC:src/%=_obj/%.o)
DEPFILES := $(OBJ:_obj/%.o=_obj/deps/%.dep)
TESTFOLDERS := $(wildcard test/*)
TESTNAMES := $(TESTFOLDERS:test/%=%)
TESTEXE := $(TESTNAMES:%=_test/%)
HEADERS := $(wildcard include/cwx/*.h)

C_CXX_FLAGS = -O2 -Wall -Wcast-qual -Wcast-function-type -Wno-array-bounds -I ~/.local -I include
CFLAGS = -std=gnu11 $(C_CXX_FLAGS) `wx-config --cflags`
CXXFLAGS = -std=gnu++11 $(C_CXX_FLAGS) `wx-config --cxxflags` -x c++
DEPFLAGS = -MT $@ -MMD -MP -MF _obj/deps/$*.$(1).dep
LDLIBS = `wx-config --libs` -L ~/.local

prefix := /usr/local
includedir := $(prefix)/include
libdir := $(prefix)/lib


.PHONY: all test run-test clean

all: $(BIN)

test: $(BIN) $(TESTEXE)

clean:
	rm -rf _bin _obj _test

$(BIN): $(OBJ) | _bin
	$(LINK.o) -o $@ $(OBJ) -shared
	chmod -x $@

_bin _obj _obj/deps _test:
	mkdir -p $@

_obj/%.c.o: src/%.c | _obj _obj/deps
	$(COMPILE.c) -o $@ $(call DEPFLAGS,c) -fPIC $<
_obj/%.cpp.o: src/%.cpp | _obj _obj/deps
	$(COMPILE.cpp) `wx-config --cxxflags` -o $@ $(call DEPFLAGS,cpp) -fPIC $<

_test/%: test/%/main.c $(BIN) | _test
	$(LINK.c) -o $@ -iquote src $< -L_bin -lcwx $(LDLIBS)

$(DEPFILES):

include $(DEPFILES)

.ONESHELL:

run-test: $(if $(test),_test/$(test),$(TESTEXE))
	@
	if [ "$(test)" ]; then
		cd "test/$(test)"; LD_LIBRARY_PATH=../../_bin ../../_test/$(test)
	else
		for test in $(TESTNAMES); do
			cd "test/$$test"; LD_LIBRARY_PATH=../../_bin ../../_test/$$test; cd ../..
		done
	fi

install: $(BIN) $(HEADERS)
	mkdir -p $(DESTDIR)$(includedir)/cwx $(DESTDIR)$(libdir)
	cp -f $(HEADERS) $(DESTDIR)$(includedir)/cwx
	cp -f $(BIN) $(DESTDIR)$(libdir)

uninstall:
	$(RM) -r $(DESTDIR)$(includedir)/cwx
	$(RM) $(BIN:_bin/%=$(DESTDIR)$(libdir)/%)
