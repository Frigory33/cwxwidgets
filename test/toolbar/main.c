#include <cwx/cwx.h>


cwxFrame * frame;

int barPositionIndex = 0, iconSizeIndex = 1;

int selectedShapeIndex = 0;


void showShapeMsg(void * arg)
{
   char const * selectedShapeStr = (char const *[]){ "Triangle", "Square", "Circle" }[selectedShapeIndex];
   cwxMessageBox("Selected shape", selectedShapeStr, frame);
}

void selectShape(int number)
{
   selectedShapeIndex = number;
}

void showToolBar(void)
{
   uint32_t const positions[] = { 0, cwxTB_BOTTOM, cwxTB_LEFT, cwxTB_RIGHT };
   int const iconSizes[] = { 16, 24, 32, 48 };

   cwxCall(frame, setToolBar, (iconSizes[iconSizeIndex], positions[barPositionIndex], cwxTools(
      cwxSimpleTool("icons/empty.svg", &showShapeMsg, .label = "Empty"),
      cwxRadioTools(&selectShape, selectedShapeIndex,
         { "icons/triangle.svg", .label = "Triangle" },
         { "icons/square.svg", .label = "Square" },
         { "icons/circle.svg", .label = "Circle" },
      ),
   )));
}


void about(void * arg)
{
   cwxMessageBox("cwxWidgets tool bar test", "This is a test program of cwxWidgets which has a tool bar.", frame);
}

void quit(void * arg)
{
   cwxCall(frame, destroy, ());
}


void changePosition(int number)
{
   barPositionIndex = number;
   showToolBar();
}

void changeIconSize(int number)
{
   iconSizeIndex = number;
   showToolBar();
}


int cwxMain(void)
{
   frame = cwxFrame("cwxWidgets tool bar", cwxSHOW);
   cwxCall(frame, appendMenus, (
      { "&Program", cwxMenuItems(
         cwxSimpleMenu("&About", &about, .hotkey = "F2", .stock = wxID_ABOUT),
         {},
         cwxSimpleMenu("&Quit", &quit, .hotkey = "Escape", .stock = wxID_EXIT),
      ) },
      { "&Tool bar", cwxMenuItems(
         cwxSimpleMenu("&Show at", cwxMenuItems(
            cwxRadioMenus(&changePosition, barPositionIndex,
               { "Top", .hotkey = "Ctrl-Up" },
               { "Bottom", .hotkey = "Ctrl-Down" },
               { "Left", .hotkey = "Ctrl-Left" },
               { "Right", .hotkey = "Ctrl-Right" },
            ),
         )),
         cwxSimpleMenu("Set icon si&ze to", cwxMenuItems(
            cwxRadioMenus(&changeIconSize, iconSizeIndex,
               { "16", .hotkey = "Ctrl-1" },
               { "24", .hotkey = "Ctrl-2" },
               { "32", .hotkey = "Ctrl-3" },
               { "48", .hotkey = "Ctrl-4" },
            ),
         )),
      ) },
   ));
   showToolBar();

   return EXIT_SUCCESS;
}
