#include <cwx/cwx.h>


cwxFrame * frame;


void hey(void * arg)
{
   cwxMessageBox("How are you?", "This is cwxWidgets.", frame);
}

void quit(void * arg)
{
   cwxCall(frame, destroy, ());
}


void about(void * arg)
{
   cwxMessageBox("Hello with cwxWidgets", "This is a very simple program to demonstrate cwxWidgets.", frame);
}


int cwxMain(void)
{
   frame = cwxFrame("Hello", cwxSHOW, .statusFields = 1);
   cwxCall(frame, appendMenus, (
      { "&Hello", cwxMenuItems(
         cwxSimpleMenu("&Greetings", &hey, .hotkey = "Space", .help = "Hey!!"),
         {},
         cwxSimpleMenu("&Quit", &quit, .hotkey = "Escape", .stock = wxID_EXIT, .help = "No more than a goodbye, please."),
      ) },
      { "Hel&p", cwxMenuItems(
         cwxSimpleMenu("&About", &about, .hotkey = "F2", .stock = wxID_ABOUT,
            .help = "Show some explanation about what this so cool program actually is"),
      ) },
   ));
   cwxCall(frame, status, ("Got it!"));

   return EXIT_SUCCESS;
}
