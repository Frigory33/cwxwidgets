#ifndef CWX_FRAME_H
#define CWX_FRAME_H


#include "menus.h"
#include "toolbar.h"


enum {
   cwxSHOW = 1,
};


Struct(wxFrame)
Struct(cwxFrameMethods)

Struct(cwxFrame, {
   wxFrame * obj;
   cwxFrameMethods const * methods;
})

DeclArgs(void, cwxMeth_status, (cwxFrame * frame; char const * text; int number))

Struct(cwxFrameMethods, {
   void (* cwxMeth_destroy)(cwxFrame * frame);
   DeclFuncPtr(cwxMeth_status)
   void (* cwxMeth_appendMenus)(cwxFrame * frame, cwxMenuBarDesc menusDesc);
   void (* cwxMeth_setToolBar)(cwxFrame * frame, cwxToolBarDesc toolBarDesc);
})

DeclFunc(cwxFrame *, cwxFrame, (char const * title; uint32_t flags; cwxFrame * parent; int statusFields))
#define cwxFrame(title, ...) call_func(cwxFrame, (.flags = 0), (title, __VA_ARGS__))

#define cwxMeth_destroy(frame, nothing) cwxMeth_destroy(frame nothing)
#define cwxMeth_status(frame, text, ...) call_func(cwxMeth_status, (.number = 0), (frame, text, __VA_ARGS__))
#define cwxMeth_appendMenus(frame, ...) cwxMeth_appendMenus(frame, (cwxMenuBarDesc){ cwxMenus(__VA_ARGS__) })
#define cwxMeth_setToolBar(frame, ...) cwxMeth_setToolBar(frame, (cwxToolBarDesc){ __VA_ARGS__ })


#endif
