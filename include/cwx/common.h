#ifndef CWX_DEFS_H
#define CWX_DEFS_H


#include <wx/defs.h>

#include <handymacros.h>


#define cwxCall(obj, meth, args) call_meth(obj, cwxMeth_ ## meth, args)


#endif
