#ifndef CWX_MENUS_H
#define CWX_MENUS_H


#include "common.h"


Struct(cwxMenuItemDesc)
DeclareSlice(cwxMenuItemDesc)

Struct(cwxSimpleMenuItemDesc, {
   char const * label;
   void (* action)(void * arg);
   void * arg;
   char const * hotkey, * help;
   int stock;
   bool checkable, checked;
   Slice(cwxMenuItemDesc) subsDesc;
})

Struct(cwxRadioMenuItemDesc, {
   char const * label, * hotkey, * help;
   int stock;
})
DeclareSlice(cwxRadioMenuItemDesc)

Struct(cwxRadioMenuItemsDesc, {
   void (* action)(int number);
   int selected;
   Slice(cwxRadioMenuItemDesc) descs;
})

enum { cwxMENUDESC_SEPARATOR, cwxMENUDESC_SIMPLE, cwxMENUDESC_RADIO };

struct cwxMenuItemDesc {
   int type;
   union {
      cwxSimpleMenuItemDesc simple;
      cwxRadioMenuItemsDesc radio;
   };
};

Struct(cwxMenuDesc, {
   char const * label;
   Slice(cwxMenuItemDesc) subsDesc;
})
DeclareSlice(cwxMenuDesc)

Struct(cwxMenuBarDesc, {
   Slice(cwxMenuDesc) subsDesc;
})

#define cwxMenus(...) .subsDesc = slice_on_values(cwxMenuDesc, __VA_ARGS__)
#define cwxMenuItems(...) .subsDesc = slice_on_values(cwxMenuItemDesc, __VA_ARGS__)
#define cwxSimpleMenu(...) { cwxMENUDESC_SIMPLE, { .simple = { __VA_ARGS__ } } }
#define cwxRadioMenus(action, selected, ...) \
   { cwxMENUDESC_RADIO, { .radio = { action, selected, slice_on_values(cwxRadioMenuItemDesc, __VA_ARGS__) } } }


#endif
