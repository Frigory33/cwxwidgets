#ifndef CWX_TOOLBAR_H
#define CWX_TOOLBAR_H


#include "common.h"


enum {
   cwxTB_BOTTOM = 1,
   cwxTB_LEFT = 2,
   cwxTB_RIGHT = 4,
};


Struct(cwxSimpleToolDesc, {
   char const * iconPath;
   void (* action)(void * arg);
   void * arg;
   char const * label, * help;
})

Struct(cwxRadioToolDesc, {
   char const * iconPath, * label, * help;
})
DeclareSlice(cwxRadioToolDesc)

Struct(cwxRadioToolsDesc, {
   void (* action)(int number);
   int selected;
   Slice(cwxRadioToolDesc) descs;
})

enum { cwxTOOLDESC_SEPARATOR, cwxTOOLDESC_SIMPLE, cwxTOOLDESC_RADIO };

Struct(cwxToolDesc, {
   int type;
   union {
      cwxSimpleToolDesc simple;
      cwxRadioToolsDesc radio;
   };
})
DeclareSlice(cwxToolDesc)

Struct(cwxToolBarDesc, {
   int iconSize;
   uint32_t flags;
   Slice(cwxToolDesc) toolsDesc;
})

#define cwxTools(...) .toolsDesc = slice_on_values(cwxToolDesc, __VA_ARGS__)
#define cwxSimpleTool(...) { cwxTOOLDESC_SIMPLE, { .simple = { __VA_ARGS__ } } }
#define cwxRadioTools(action, selected, ...) \
   { cwxTOOLDESC_RADIO, { .radio = { action, selected, slice_on_values(cwxRadioToolDesc, __VA_ARGS__) } } }


#endif
