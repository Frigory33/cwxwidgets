#ifndef CWX_MISC_H
#define CWX_MISC_H


#include "common.h"


Struct(cwxFrame)


DeclFunc(int, cwxMessageBox, (char const * title, * content; cwxFrame * parent))
#define cwxMessageBox(title, content, ...) call_func(cwxMessageBox, (nullptr), (title, content, __VA_ARGS__))


#endif
