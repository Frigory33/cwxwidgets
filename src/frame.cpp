#include "common.hpp"

#include <cwx/frame.h>

#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/toolbar.h>


static wxIcon getIconOfSize(char const * path, int size)
{
   wxIcon icon{ path };
   if (!icon.IsOk() || icon.GetWidth() == size || icon.GetHeight() == size) {
      return icon;
   }
   wxBitmap bmp;
   bmp.CopyFromIcon(icon);
   wxImage img = bmp.ConvertToImage();
   img.Rescale(size, size, wxIMAGE_QUALITY_BICUBIC);
   icon.CopyFromBitmap(img);
   return icon;
}


FUNC void cwxFrame_destroy(cwxFrame * frame)
{
   frame->obj->Destroy();
   free(frame);
}


DefFunc2(cwxFrame_status, cwxMeth_status)
{
   arg.frame->obj->SetStatusText(Str(arg.text), arg.number);
}


static void appendMenus_rec(cwxFrame * frame, wxMenu * menu, Slice(cwxMenuItemDesc) itemsDesc)
{
   for_slice (cwxMenuItemDesc, subDesc, itemsDesc) {
      switch (subDesc.type) {
         case cwxMENUDESC_SEPARATOR:
            menu->AppendSeparator();
            break;
         case cwxMENUDESC_SIMPLE: {
            cwxSimpleMenuItemDesc itemDesc = subDesc.simple;
            wxString label = NullableStr(itemDesc.label);
            if (itemDesc.subsDesc.len != 0) {
               wxMenu * subMenu = new wxMenu();
               appendMenus_rec(frame, subMenu, itemDesc.subsDesc);
               menu->AppendSubMenu(subMenu, label, NullableStr(itemDesc.help));
            } else {
               if (itemDesc.hotkey != nullptr && itemDesc.label != nullptr) {
                  label << '\t' << Str(itemDesc.hotkey);
               }
               wxMenuItem * item = menu->Append(itemDesc.stock != 0 ? itemDesc.stock : wxID_ANY, label, NullableStr(itemDesc.help));
               if (itemDesc.action != nullptr) {
                  frame->obj->Bind(wxEVT_MENU, &cwxEventHandler, item->GetId(), wxID_ANY,
                     new cwxHandler(itemDesc.action, itemDesc.arg));
               }
            }
         }  break;
         case cwxMENUDESC_RADIO: {
            int number = 0;
            for_slice (cwxRadioMenuItemDesc, itemDesc, subDesc.radio.descs) {
               wxString label = NullableStr(itemDesc.label);
               if (itemDesc.hotkey != nullptr && itemDesc.label != nullptr) {
                  label << '\t' << Str(itemDesc.hotkey);
               }
               wxMenuItem * item = menu->AppendRadioItem(itemDesc.stock != 0 ? itemDesc.stock : wxID_ANY, label,
                     NullableStr(itemDesc.help));
               if (subDesc.radio.selected == number) {
                  menu->Check(item->GetId(), true);
               }
               if (subDesc.radio.action != nullptr) {
                  frame->obj->Bind(wxEVT_MENU, &cwxRadioEventHandler, item->GetId(), wxID_ANY,
                     new cwxRadioHandler(subDesc.radio.action, number));
               }
               ++number;
            }
         }  break;
      }
   }
}

FUNC void cwxFrame_appendMenus(cwxFrame * frame, cwxMenuBarDesc menusDesc)
{
   wxMenuBar * menuBar = frame->obj->GetMenuBar();
   if (menuBar == nullptr) {
      menuBar = new wxMenuBar();
   }
   for_slice (cwxMenuDesc, menuDesc, menusDesc.subsDesc) {
      if (menuDesc.label != nullptr) {
         wxMenu * menu = new wxMenu();
         appendMenus_rec(frame, menu, menuDesc.subsDesc);
         menuBar->Append(menu, Str(menuDesc.label));
      }
   }
   frame->obj->SetMenuBar(menuBar);
}


FUNC void cwxFrame_setToolBar(cwxFrame * frame, cwxToolBarDesc toolBarDesc)
{
   delete frame->obj->GetToolBar();
   if (toolBarDesc.toolsDesc.len == 0) {
      return;
   }
   long style =
      (toolBarDesc.flags & cwxTB_BOTTOM) != 0 ? wxTB_HORIZONTAL | wxTB_BOTTOM :
      (toolBarDesc.flags & cwxTB_LEFT) != 0 ? wxTB_VERTICAL :
      (toolBarDesc.flags & cwxTB_RIGHT) != 0 ? wxTB_VERTICAL | wxTB_RIGHT : wxTB_HORIZONTAL;
   wxToolBar * toolBar = frame->obj->CreateToolBar(style);
   for_slice (cwxToolDesc, toolDesc, toolBarDesc.toolsDesc) {
      switch (toolDesc.type) {
         case cwxTOOLDESC_SEPARATOR:
            toolBar->AddSeparator();
            break;
         case cwxTOOLDESC_SIMPLE: {
            wxIcon icon = getIconOfSize(toolDesc.simple.iconPath, toolBarDesc.iconSize);
            wxString label = NullableStr(toolDesc.simple.label);
            wxToolBarToolBase * tool = toolBar->AddTool(wxID_ANY, label, wxBitmapBundle(icon),
                  toolDesc.simple.help == nullptr ? label : Str(toolDesc.simple.help));
            if (toolDesc.simple.action != nullptr) {
               frame->obj->Bind(wxEVT_TOOL, &cwxEventHandler, tool->GetId(), wxID_ANY,
                  new cwxHandler(toolDesc.simple.action, toolDesc.simple.arg));
            }
         }  break;
         case cwxTOOLDESC_RADIO: {
            int number = 0;
            for_slice (cwxRadioToolDesc, radioToolDesc, toolDesc.radio.descs) {
               wxIcon icon = getIconOfSize(radioToolDesc.iconPath, toolBarDesc.iconSize);
               wxString label = NullableStr(radioToolDesc.label);
               wxToolBarToolBase * tool = toolBar->AddRadioTool(wxID_ANY, label, wxBitmapBundle(icon),
                     wxNullBitmap, radioToolDesc.help == nullptr ? label : Str(radioToolDesc.help));
               if (toolDesc.radio.selected == number) {
                  toolBar->ToggleTool(tool->GetId(), true);
               }
               if (toolDesc.radio.action != nullptr) {
                  frame->obj->Bind(wxEVT_TOOL, &cwxRadioEventHandler, tool->GetId(), wxID_ANY,
                     new cwxRadioHandler(toolDesc.radio.action, number));
               }
               ++number;
            }
         }  break;
      }
   }
}


static cwxFrameMethods const frameMethods = { &cwxFrame_destroy, func_addr(cwxFrame_status), &cwxFrame_appendMenus,
   &cwxFrame_setToolBar };


DefFunc(cwxFrame)
{
   InitPtr(cwxFrame, frame, {
         new wxFrame(arg.parent == nullptr ? nullptr : arg.parent->obj, wxID_ANY, arg.title),
         &frameMethods,
      });
   if (arg.statusFields > 0) {
      frame->obj->CreateStatusBar(arg.statusFields);
   }
   if (arg.flags & cwxSHOW) {
      frame->obj->Show(true);
   }
   return frame;
}
