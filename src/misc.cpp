#include "common.hpp"

#include <cwx/misc.h>
#include <cwx/frame.h>

#include <wx/msgdlg.h>
#include <wx/frame.h>


extern inline wxString Str(char const * str);
extern inline wxString NullableStr(char const * str);


void cwxEventHandler(wxCommandEvent & evt)
{
   cwxHandler * handler = dynamic_cast<cwxHandler *>(evt.GetEventUserData());
   (*handler->func)(handler->arg);
}

void cwxRadioEventHandler(wxCommandEvent & evt)
{
   cwxRadioHandler * handler = dynamic_cast<cwxRadioHandler *>(evt.GetEventUserData());
   (*handler->func)(handler->number);
}


DefFunc(cwxMessageBox)
{
   return wxMessageBox(Str(arg.content), Str(arg.title), wxOK | wxCENTRE, arg.parent == nullptr ? nullptr : arg.parent->obj);
}
