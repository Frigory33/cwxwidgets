#include "common.hpp"

#include <wx/app.h>


FUNC int cwxMain(int argc, char * argv[]);

class cwxApp : public wxApp {
   public:
      bool OnInit() override;
};

bool cwxApp::OnInit()
{
   return cwxMain(argc, argv) == EXIT_SUCCESS;
}

wxIMPLEMENT_APP(cwxApp);
