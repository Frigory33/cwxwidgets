#ifndef CWXWIDGETS_COMMON_HPP
#define CWXWIDGETS_COMMON_HPP


#include <handymacros.h>

#include <wx/string.h>
#include <wx/event.h>


inline wxString Str(char const * str)
{ return wxString::FromUTF8(str); }

inline wxString NullableStr(char const * str)
{ return str == nullptr ? wxString("") : Str(str); }


struct cwxHandler : wxObject {
   void (* func)(void * arg);
   void * arg;
   cwxHandler(void (* func)(void * arg), void * arg) : func(func), arg(arg) {}
};

struct cwxRadioHandler : wxObject {
   void (* func)(int number);
   int number;
   cwxRadioHandler(void (* func)(int number), int number) : func(func), number(number) {}
};

void cwxEventHandler(wxCommandEvent & evt);
void cwxRadioEventHandler(wxCommandEvent & evt);


#endif
